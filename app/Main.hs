module Main where

import Api.Handler(handler)
import Network.Wai.Handler.Warp(run)

main :: IO ()
main = run port handler
  where
    port = 8080