module Api.Handler
  ( handler
  ) where

import Servant(serve, Proxy(..), Server)
import Network.Wai(Application)

import Api.Router
import Api.Users.Functions

-- FIXME:
-- server = return getUserList
-- must have diffrent return base on request type

server :: Server UserAPI
server = return getUserList

userAPI :: Proxy UserAPI
userAPI = Proxy

handler :: Application
handler = serve userAPI server
    