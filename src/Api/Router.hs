{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api.Router where 
  
import Servant(serve, Proxy(..), Server, JSON, Get, (:>))

import Api.Users.Types
import Api.Users.Functions

type UserAPI = "users" :> Get '[JSON] [User]