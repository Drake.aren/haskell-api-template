module Api.Users.Functions 
  ( getUserList
  ) where 

import Api.Users.Types

getUserList :: [User]
getUserList =
  [ User "Isaac Newton"    "isaac@newton.co.uk"
  , User "Albert Einstein" "ae@mc2.org"
  ]
