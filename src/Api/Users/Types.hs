{-# LANGUAGE DeriveGeneric #-}

module Api.Users.Types where

import GHC.Generics(Generic)
import Data.Aeson(ToJSON)

data User = User
  { name :: String
  , email :: String
  } deriving (Eq, Show, Generic)

instance ToJSON User